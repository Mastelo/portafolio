<?php
include "../../conn.php";

$user_id=null;
$sql1= "select * from usuario where ID_USUARIO = ".$_GET["id"];
$query = $conn->query($sql1);
$person = null;
if($query->num_rows>0){
while ($r=$query->fetch_object()){
  $person=$r;
  break;
}

  }
?>

<?php if($person!=null):?>

<html>
  <head>
    <title> TIS </title>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../js/jquery.min.js"></script>
  </head>
  <body>
  <?php include "navbar.php"; ?>
<div class="container">
<div class="row">
<div class="col-md-6">
    <h2>EDITAR</h2>






<form role="form" method="post" action="actest.php">
  <div class="form-group">
    <label for="name">Nombres</label>
    <input type="text" class="form-control" value="<?php echo $person->NOMBRES; ?>" name="name" required>
  </div>
  <div class="form-group">
    <label for="lastname">Apellidos</label>
    <input type="text" class="form-control" value="<?php echo $person->APELLIDOS; ?>" name="lastname" required>
  </div>
  <div class="form-group">
    <label for="address">Telefono</label>
    <input type="text" class="form-control" value="<?php echo $person->TELEFONO; ?>" name="address" required>
  </div>
  <div class="form-group">
    <label for="email">Correo</label>
    <input type="email" class="form-control" value="<?php echo $person->CORREO; ?>" name="email" >
  </div>
  <div class="form-group">
    <label for="phone">Direccion</label>
    <input type="text" class="form-control" value="<?php echo $person->DIRECCION; ?>" name="phone" >
  </div>
<input type="hidden" name="id" value="<?php echo $person->ID_USUARIO; ?>">
  <button type="submit" class="btn btn-default">Actualizar</button>
</form>
<?php else:?>
  <p class="alert alert-danger">404 No se encuentra</p>
<?php endif;?>

</div>
</div>
</div>

<script src="../../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>