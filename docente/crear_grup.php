
<!doctype html>
<html lang="en">
  <head>
    <title>Crear grupo</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
<body>

<div class="container">


	<?php
	
	include '../conn.php';

	session_start();
	
	$corre = $_SESSION['correo']; 
	
 	
	/*$sql1 = "SELECT * FROM usuario WHERE CORREO = '$corre'";
	$result1 = mysqli_query($conn, $sql1);
	$usuario = mysqli_fetch_assoc($result1);*/

	$id_usu = $_SESSION['id'];


	$name = $_POST['name'];
	$des = $_POST['descripcion'];
	$codgru = $_POST['codgru'];
	
	if(isset($_POST['guardar'])){
	
		//aumentar combobox para elegir materia
		$query = "INSERT INTO grupo (ID_USUARIO, NOMBRE, ID_MATERIA,codgru) VALUES ($id_usu, '$name',1,$codgru)";
		//aumentar combo box para elegir la materia y no escribir 1 como en la consulta
		if (mysqli_query($conn, $query)) {
			echo "<div class='alert alert-success mt-4' role='alert'><h3> Grupo creado exitosamente.</h3>
			<a class='btn btn-outline-primary' href='dashboard.php' role='button'>Volver a dashboard</a></div>";	
		} else {
			echo "Error: " . $query . "<br>" . mysqli_error($conn);
		}	
		
	}
	//mysqli_close($conn);
	?>

</div>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>