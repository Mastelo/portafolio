<?php 

include '../conn.php';
include 'dashboard.php';
 
  $id = $_SESSION['id']; 
  


  $sql = "SELECT NOMBRES, APELLIDOS, NOMBRE_MATERIA,NOMBRE_GRUPO,HORA_ENTRADA, HORA_SALIDA FROM usuario u, docente d, grupo g,materia m,horario h where u.ID_USUARIO=d.ID_USUARIO and d.ID_USUARIO= g.DOC_ID_USUARIO AND g.ID_MATERIA= m.ID_MATERIA AND g.ID_GRUPO= h.ID_GRUPO AND D.ID_USUARIO= $id";
 
  $query = $conn->query($sql);
 
  

  if (mysqli_query($conn, $sql)) {
            }
            else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }

?>
<!DOCTYPE html>
<html>

<body>
<div class="content">
        <div id="pad-wrapper" class="form-page">
            <div class="row header">
                <h3>Mis Grupos </h3>
            </div>
<table class="table table-bordered table-hover">

<thead>
    <th>NOMBRE</th>
    <th>APELLIDOS</th>
    <th>MATERIA</th>
    <th>GRUPO</th>
    <th>HORA ENTRADA</th>
    <th>HORA SALIDA</th>
    <th>OPCIONES</th>
</thead>
<tbody>
  <?php while ($row=$query->fetch_array()):?>
    <tr>
      <td><?php echo $row['NOMBRES']; ?>  </td>
      <td><?php echo $row['APELLIDOS']; ?></td>
      <td><?php echo $row['NOMBRE_MATERIA']; ?></td>
      <td><?php echo $row['NOMBRE_GRUPO']; ?></td>
      <td><?php echo $row['HORA_ENTRADA']; ?></td>
      <td><?php echo $row['HORA_SALIDA']; ?></td>
      <td style="width:150px;">
        <a href="./formedit.php?id=<?php echo $r["ID_USUARIO"];?>" class="btn btn-sm btn-warning">Editar</a>
        <a href="#" id="del-<?php echo $r["ID_USUARIO"];?>" class="btn btn-sm btn-danger">Eliminar</a>
        <script>
        $("#del-"+<?php echo $r["ID_USUARIO"];?>).click(function(e){
          e.preventDefault();
          p = confirm("Estas seguro?");
          if(p){
            window.location="elimest.php?id="+<?php echo $r["ID_USUARIO"];?>;

          }

        });
        </script>
      </td>
    </tr>

 <?php endwhile;?>
</tbody>
</table>


</body>
</html>
