-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-05-2019 a las 21:16:28
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laboro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adaministrador`
--

CREATE TABLE `adaministrador` (
  `ID_USUARIO` int(11) NOT NULL,
  `COD_ADMIN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auxiliar`
--

CREATE TABLE `auxiliar` (
  `ID_USUARIO` int(11) NOT NULL,
  `COD_AUXILIAR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auxiliar`
--

INSERT INTO `auxiliar` (`ID_USUARIO`, `COD_AUXILIAR`) VALUES
(3, 777),
(4, 888);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `ID_USUARIO` int(11) NOT NULL,
  `COD_DOCENTE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`ID_USUARIO`, `COD_DOCENTE`) VALUES
(5, 555),
(6, 333);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `ID_USUARIO` int(11) NOT NULL,
  `COD_SYS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`ID_USUARIO`, `COD_SYS`) VALUES
(1, 0),
(2, 201),
(3, 1),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante_grupo`
--

CREATE TABLE `estudiante_grupo` (
  `ID_USUARIO` int(11) NOT NULL,
  `COD_SYS` int(11) NOT NULL,
  `ID_GRUPO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiante_grupo`
--

INSERT INTO `estudiante_grupo` (`ID_USUARIO`, `COD_SYS`, `ID_GRUPO`) VALUES
(2, 201, 2),
(2, 201, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `ID_GRUPO` int(11) NOT NULL,
  `ID_MATERIA` int(11) DEFAULT NULL,
  `ID_USUARIO` int(11) DEFAULT NULL,
  `COD_AUXILIAR` int(11) DEFAULT NULL,
  `DOC_ID_USUARIO` int(11) DEFAULT NULL,
  `COD_DOCENTE` int(11) DEFAULT NULL,
  `NOMBRE_GRUPO` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`ID_GRUPO`, `ID_MATERIA`, `ID_USUARIO`, `COD_AUXILIAR`, `DOC_ID_USUARIO`, `COD_DOCENTE`, `NOMBRE_GRUPO`) VALUES
(1, 1, 3, 777, 6, 333, '3a'),
(2, 2, 3, 777, 5, 555, '1'),
(3, 1, 4, 888, 5, 555, '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `ID_GRUPO` int(11) DEFAULT NULL,
  `HORA_ENTRADA` datetime DEFAULT NULL,
  `HORA_SALIDA` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`ID_GRUPO`, `HORA_ENTRADA`, `HORA_SALIDA`) VALUES
(3, '2019-05-06 08:15:00', '2019-05-06 12:45:00'),
(2, '2019-05-06 08:15:00', '2019-05-06 09:45:00'),
(1, '2019-05-06 08:15:00', '2019-05-06 12:45:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `ID_MATERIA` int(11) NOT NULL,
  `NOMBRE_MATERIA` varchar(45) DEFAULT NULL,
  `CODIGO_MATERIA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`ID_MATERIA`, `NOMBRE_MATERIA`, `CODIGO_MATERIA`) VALUES
(1, 'Introduccion a la programacion', 123),
(2, 'Elementos de programacion y estructuras de da', 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE `portafolio` (
  `ID_PORTAFOLIO` int(11) NOT NULL,
  `ID_USUARIO` int(11) NOT NULL,
  `COD_SYS` int(11) NOT NULL,
  `ID_GRUPO` int(11) NOT NULL,
  `NOMBRE_PORTAFOLIO` varchar(20) DEFAULT NULL,
  `FECHA_CREACION_PORTA` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practica`
--

CREATE TABLE `practica` (
  `ID_PRACTICA` int(11) NOT NULL,
  `ID_GRUPO` int(11) DEFAULT NULL,
  `NOMBRE_PRACTICA` varchar(50) DEFAULT NULL,
  `HORA_SUBIDA` datetime DEFAULT NULL,
  `NOTA_PRACTICA` char(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `ID_ROL` int(11) NOT NULL,
  `ROL` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`ID_ROL`, `ROL`) VALUES
(1, 'Administrador'),
(2, 'Docente'),
(3, 'Auxiliar'),
(4, 'Estudiante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `ID_SESION` int(11) NOT NULL,
  `ID_USUARIO` int(11) NOT NULL,
  `COD_SYS` int(11) NOT NULL,
  `HORA_CONECT` datetime DEFAULT NULL,
  `HORA_DESCON` datetime NOT NULL,
  `ASISTENCIA` tinyint(1) DEFAULT NULL,
  `ACTIVIDADES` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`ID_SESION`, `ID_USUARIO`, `COD_SYS`, `HORA_CONECT`, `HORA_DESCON`, `ASISTENCIA`, `ACTIVIDADES`) VALUES
(1, 2, 201, '2019-05-05 14:26:00', '2019-05-05 14:15:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ID_USUARIO` int(11) NOT NULL,
  `ID_ROL` int(11) DEFAULT NULL,
  `CORREO` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(200) DEFAULT NULL,
  `NOMBRES` varchar(150) DEFAULT NULL,
  `APELLIDOS` varchar(150) DEFAULT NULL,
  `TELEFONO` int(11) DEFAULT NULL,
  `DIRECCION` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ID_USUARIO`, `ID_ROL`, `CORREO`, `PASSWORD`, `NOMBRES`, `APELLIDOS`, `TELEFONO`, `DIRECCION`) VALUES
(1, 1, 'adm@gmail.com', '$2y$10$q8r96ccP/flq3Zw2N4s36.WoI1NNrFUAXEJjRzSqq/o9Ec1MfbVZy', 'Lui', 'Cuellar', 777, 'capitan ustariz'),
(2, 4, 'est@gmail.com', '$2y$10$gnKdzr4YtYNBSFY3OL6hFuSHXqvft3WD4ZABufzXTDkR5.jcG7E4C', 'Ronald', 'Miranda', 777, 'capitan ustariz'),
(3, 3, 'aux@gmail.com', '$2y$10$L9HQS4h.z5vAlXujPcYXY.RnjD9qAY8Sr/oRPmb3sfAKUn8COi9SS', 'Valen', 'Rai', 777, 'buenos aires'),
(4, 3, 'aux1@gmail.com', '$2y$10$Ga03YuvbKZQ4iL3/Ci0X4.2IEvvRxGjHzHdKSpZKeqDwZSXjyqen2', 'Lui', 'Cuellar', 777, 'capitan ustariz'),
(5, 2, 'doc@gmail.com', '$2y$10$pLh10VzBRhkiTyEj6fAW3OLDkjpvLQxxN4dPslOEc6aFPkzl5Mr3O', 'Valen', 'Rai', 777, 'daniel campos'),
(6, 2, 'doc1@gmail.com', '$2y$10$J0whp4u0KCyWACc3HOwcZeMcL5uGUU2Y/d8wncAkGU3g9s25mR75.', 'Estela', 'doc', 777, 'capitan ustariz');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adaministrador`
--
ALTER TABLE `adaministrador`
  ADD PRIMARY KEY (`ID_USUARIO`,`COD_ADMIN`);

--
-- Indices de la tabla `auxiliar`
--
ALTER TABLE `auxiliar`
  ADD PRIMARY KEY (`ID_USUARIO`,`COD_AUXILIAR`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`ID_USUARIO`,`COD_DOCENTE`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`ID_USUARIO`,`COD_SYS`);

--
-- Indices de la tabla `estudiante_grupo`
--
ALTER TABLE `estudiante_grupo`
  ADD PRIMARY KEY (`ID_USUARIO`,`COD_SYS`,`ID_GRUPO`),
  ADD KEY `FK_RELATIONSHIP_20` (`ID_GRUPO`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`ID_GRUPO`),
  ADD KEY `FK_RELATIONSHIP_14` (`ID_MATERIA`),
  ADD KEY `FK_RELATIONSHIP_17` (`DOC_ID_USUARIO`,`COD_DOCENTE`),
  ADD KEY `FK_RELATIONSHIP_18` (`ID_USUARIO`,`COD_AUXILIAR`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD KEY `FK_RELATIONSHIP_24` (`ID_GRUPO`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`ID_MATERIA`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`ID_PORTAFOLIO`),
  ADD KEY `FK_RELATIONSHIP_23` (`ID_USUARIO`,`COD_SYS`,`ID_GRUPO`);

--
-- Indices de la tabla `practica`
--
ALTER TABLE `practica`
  ADD PRIMARY KEY (`ID_PRACTICA`),
  ADD KEY `FK_RELATIONSHIP_16` (`ID_GRUPO`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`ID_ROL`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`ID_SESION`),
  ADD KEY `FK_RELATIONSHIP_15` (`ID_USUARIO`,`COD_SYS`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID_USUARIO`),
  ADD KEY `FK_RELATIONSHIP_7` (`ID_ROL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `ID_USUARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `ID_GRUPO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `ID_MATERIA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `ID_PORTAFOLIO` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `practica`
--
ALTER TABLE `practica`
  MODIFY `ID_PRACTICA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `ID_ROL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sesion`
--
ALTER TABLE `sesion`
  MODIFY `ID_SESION` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID_USUARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adaministrador`
--
ALTER TABLE `adaministrador`
  ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`);

--
-- Filtros para la tabla `auxiliar`
--
ALTER TABLE `auxiliar`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`);

--
-- Filtros para la tabla `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`);

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`);

--
-- Filtros para la tabla `estudiante_grupo`
--
ALTER TABLE `estudiante_grupo`
  ADD CONSTRAINT `FK_RELATIONSHIP_19` FOREIGN KEY (`ID_USUARIO`,`COD_SYS`) REFERENCES `estudiante` (`ID_USUARIO`, `COD_SYS`),
  ADD CONSTRAINT `FK_RELATIONSHIP_20` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`);

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `FK_RELATIONSHIP_14` FOREIGN KEY (`ID_MATERIA`) REFERENCES `materia` (`ID_MATERIA`),
  ADD CONSTRAINT `FK_RELATIONSHIP_17` FOREIGN KEY (`DOC_ID_USUARIO`,`COD_DOCENTE`) REFERENCES `docente` (`ID_USUARIO`, `COD_DOCENTE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_18` FOREIGN KEY (`ID_USUARIO`,`COD_AUXILIAR`) REFERENCES `auxiliar` (`ID_USUARIO`, `COD_AUXILIAR`);

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `FK_RELATIONSHIP_24` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`);

--
-- Filtros para la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD CONSTRAINT `FK_RELATIONSHIP_23` FOREIGN KEY (`ID_USUARIO`,`COD_SYS`,`ID_GRUPO`) REFERENCES `estudiante_grupo` (`ID_USUARIO`, `COD_SYS`, `ID_GRUPO`);

--
-- Filtros para la tabla `practica`
--
ALTER TABLE `practica`
  ADD CONSTRAINT `FK_RELATIONSHIP_16` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`);

--
-- Filtros para la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD CONSTRAINT `FK_RELATIONSHIP_15` FOREIGN KEY (`ID_USUARIO`,`COD_SYS`) REFERENCES `estudiante` (`ID_USUARIO`, `COD_SYS`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`ID_ROL`) REFERENCES `rol` (`ID_ROL`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
