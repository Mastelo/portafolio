<?php 

include '../conn.php';
  session_start();
  $id = $_SESSION['id']; 
  

  $sql = "SELECT * FROM practica";
//  $sql = "SELECT * FROM practica where ID_USUARIO = '$id'";

  $result = mysqli_query($conn, $sql);
 // $r = mysqli_fetch_all($result, MYSQLI_ASSOC);
  
  if (mysqli_query($conn, $sql)) {

                echo "Sesiones registradas<div class='alert alert-danger mt-4' role='alert'><p><a href='dashboard.php'><strong></strong></a></p> </div>";
            }
            else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
?>


<?php if($result->num_rows>0):?>
<table class="table table-bordered table-hover">
<thead>
  <th>Id usuario</th>
  <th>codigo auxiliar</th>
  <th>Nombre de la practica</th>
  <th>Hora de subida</th>
  <th>Nota</th>
  <th>Observacion</th>
  
  <th></th>
</thead>
<?php while ($r=$result->fetch_array()):?>
<tr>
  <td><?php echo $r["ID_USUARIO"]; ?></td>
  <td><?php echo $r["COD_AUXILIAR"]; ?></td>
  <td><?php echo $r["NOMBRE_PRACTICA"]; ?></td>
  <td><?php echo $r["HORA_SUBIDA"]; ?></td>
  <td><?php echo $r["NOTA_PRACTICA"]; ?></td>
  <td><?php echo $r["OBSERVACION_PRAC"]; ?></td>
  <td style="width:150px;">
    <a href="./editarpractica.php?id=<?php echo $r["ID_PRACTICA"];?>" class="btn btn-sm btn-warning">Editar</a>
    <a href="#" id="del-<?php echo $r["ID_USUARIO"];?>" class="btn btn-sm btn-danger">Eliminar</a>
    <script>
    $("#del-"+<?php echo $r["ID_USUARIO"];?>).click(function(e){
      e.preventDefault();
      p = confirm("Estas seguro?");
      if(p){
        window.location="elimest.php?id="+<?php echo $r["ID_USUARIO"];?>;

      }

    });
    </script>
  </td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
  <p class="alert alert-warning">No hay resultados</p>
<?php endif;?>
