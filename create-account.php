<!doctype html>
<html lang="en">
  <head>
    <title>CREAR CUENTA NUEVA</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
<body>

<div class="container">

	<?php
	include 'conn.php';
	include 'geters.php';
	include 'email/email.php';

	// Consulta para comprobar si el correo electrónico ya existe.
	$checkEmail = "SELECT * FROM usuario WHERE CORREO = '$_POST[email]' ";
	// Variable $result hold the connection data and the query
	$result = $conn-> query($checkEmail);
	// Variable $count hold the result of the query
	$count = mysqli_num_rows($result);

	// If count == 1 that means the email is already on the database
	if ($count == 1) {
	echo "<div class='alert alert-warning mt-4' role='alert'>
					<p>EL CORREO YA EXISTE.</p>
					<p><a href='login.php'>Por favor intente con otro correo</a></p>
				</div>";
	} else {
	/*
		Si el correo electrónico no existe, los datos del formulario se envían a la
		Base de datos y se crea la cuenta. esta parte se conecta con los nombres de las etiquetas
	*/
	$name = $_POST['name'];
	$ape = $_POST['apellidos'];
	$dir = $_POST['direccion'];
	$telef = $_POST['telefono'];
	$tipo = $_POST['Cbousuario'];
	$email = $_POST['email'];
	$pass = $_POST['password'];
	
	// La función password_hash () convierte la contraseña en un hash antes de enviarla a la base de datos
	$passHash = password_hash($pass, PASSWORD_DEFAULT);
	
	// Consulta para enviar hash de nombre, correo electrónico y contraseña  y demas datos a la base de datos
	$query = "INSERT INTO usuario (NOMBRES, APELLIDOS, DIRECCION, TELEFONO,CORREO,PASSWORD,ID_ROL) VALUES ('$name', '$ape', '$dir', '$telef', '$email', '$passHash','$tipo')";

	if (mysqli_query($conn, $query)) {
		//if($tipo== 4){
			//hacer funcion en email y enviar todos los datos de arriba 
			enviarcorr($name,$ape,$email,$dir, $telef, $pass);

			$usuario = obtenerdatos($email);
			$id_usu = $usuario['ID_USUARIO']; 

			$dirname = "estudiante/uploads/".$id_usu;
	  		mkdir($dirname);

	  		$query = "INSERT INTO estudiante (ID_USUARIO) VALUES ('$id_usu')";
	  		if(mysqli_query($conn, $query)){
	  		
			} else {
				echo "Error: " . $query . "<br>" . mysqli_error($conn);
			}	

  		//}
			echo "<div class='alert alert-success mt-4' role='alert'><h3> Cuenta creada exitosamente, se le envio un mensaje de sus datos a su correo.</h3>
			<a class='btn btn-outline-primary' href='login.php' role='button'>Login</a></div>";	

	} else {
		echo "Error: " . $query . "<br>" . mysqli_error($conn);
	}	
	}	
	mysqli_close($conn);
	?>
</div>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>