/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     5/6/2019 2:09:16 PM                          */
/*==============================================================*/


drop table if exists ADMINISTRADOR;

drop table if exists AUXILIAR;

drop table if exists DOCENTE;

drop table if exists DOCENTE_MATERIA;

drop table if exists ESTUDIANTE;

drop table if exists ESTUDIANTE_GRUPO;

drop table if exists ESTUDIANTE_PRACTICA;

drop table if exists GRUPO;

drop table if exists HORARIO;

drop table if exists MATERIA;

drop table if exists PRACTICA;

drop table if exists PRACTICA_TAREA;

drop table if exists ROL;

drop table if exists SESION;

drop table if exists USUARIO;

/*==============================================================*/
/* Table: ADMINISTRADOR                                        */
/*==============================================================*/
create table ADMINISTRADOR
(
   ID_USUARIO           int not null,
   COD_ADMIN            int not null,
   primary key (ID_USUARIO, COD_ADMIN)
);

/*==============================================================*/
/* Table: AUXILIAR                                              */
/*==============================================================*/
create table AUXILIAR
(
   ID_USUARIO           int not null,
   COD_AUXILIAR         int not null,
   primary key (ID_USUARIO, COD_AUXILIAR)
);

/*==============================================================*/
/* Table: DOCENTE                                               */
/*==============================================================*/
create table DOCENTE
(
   ID_USUARIO           int not null,
   COD_DOCENTE          int not null,
   primary key (ID_USUARIO, COD_DOCENTE)
);

/*==============================================================*/
/* Table: DOCENTE_MATERIA                                       */
/*==============================================================*/
create table DOCENTE_MATERIA
(
   ID_USUARIO           int,
   COD_DOCENTE          int,
   ID_MATERIA           int
);

/*==============================================================*/
/* Table: ESTUDIANTE                                            */
/*==============================================================*/
create table ESTUDIANTE
(
   ID_USUARIO           int not null,
   COD_SYS              int not null,
   primary key (ID_USUARIO, COD_SYS)
);

/*==============================================================*/
/* Table: ESTUDIANTE_GRUPO                                      */
/*==============================================================*/
create table ESTUDIANTE_GRUPO
(
   ID_USUARIO           int not null,
   COD_SYS              int not null,
   ID_GRUPO             int not null,
   primary key (ID_USUARIO, COD_SYS, ID_GRUPO)
);

/*==============================================================*/
/* Table: ESTUDIANTE_PRACTICA                                   */
/*==============================================================*/
create table ESTUDIANTE_PRACTICA
(
   ID_PRACTICA          int,
   ID_USUARIO           int,
   COD_SYS              int
);

/*==============================================================*/
/* Table: GRUPO                                                 */
/*==============================================================*/
create table GRUPO
(
   ID_GRUPO             int not null AUTO_INCREMENT,
   ID_MATERIA           int,
   ID_USUARIO           int,
   COD_AUXILIAR         int,
   DOC_ID_USUARIO       int,
   COD_DOCENTE          int,
   NOMBRE_GRUPO         varchar(20),
   FECHA_INI            date,
   FECHA_FIN            date,
   primary key (ID_GRUPO)
);

/*==============================================================*/
/* Table: HORARIO                                               */
/*==============================================================*/
create table HORARIO
(
   ID_GRUPO             int,
   HORA_ENTRADA         datetime,
   HORA_SALIDA          datetime
);

/*==============================================================*/
/* Table: MATERIA                                               */
/*==============================================================*/
create table MATERIA
(
   ID_MATERIA           int not null AUTO_INCREMENT,
   NOMBRE_MATERIA       varchar(20),
   CODIGO_MATERIA       int,
   primary key (ID_MATERIA)
);

/*==============================================================*/
/* Table: PRACTICA                                              */
/*==============================================================*/
create table PRACTICA
(
   ID_PRACTICA          int not null AUTO_INCREMENT,
   ID_PRACTICA_TAREA    int,
   NOMBRE_PRACTICA      varchar(50),
   HORA_SUBIDA          datetime,
   NOTA_PRACTICA        char(6),
   primary key (ID_PRACTICA)
);

/*==============================================================*/
/* Table: PRACTICA_TAREA                                        */
/*==============================================================*/
create table PRACTICA_TAREA
(
   ID_PRACTICA_TAREA    int not null AUTO_INCREMENT,
   ID_GRUPO             int,
   NOMBRE_PRACTI        varchar(50),
   HORA_FECHA_ENTREGA   datetime,
   FECHA_SUBID          datetime,
   primary key (ID_PRACTICA_TAREA)
);

/*==============================================================*/
/* Table: ROL                                                   */
/*==============================================================*/
create table ROL
(
   ID_ROL               int not null AUTO_INCREMENT,
   ROL                  varchar(100),
   primary key (ID_ROL)
);

/*==============================================================*/
/* Table: SESION                                                */
/*==============================================================*/
create table SESION
(
   ID_SESION            int not null AUTO_INCREMENT,
   ID_USUARIO           int not null,
   COD_SYS              int not null,
   HORA_CONECT          datetime,
   ASISTENCIA           bool,
   ACTIVIDADES          varchar(500),
   primary key (ID_SESION)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO
(
   ID_USUARIO           int not null AUTO_INCREMENT,
   ID_ROL               int,
   CORREO               varchar(100),
   PASSWORD             varchar(200),
   NOMBRES              varchar(150),
   APELLIDOS            varchar(150),
   TELEFONO             int,
   DIRECCION            varchar(250),
   primary key (ID_USUARIO)
);

alter table ADMINISTRADOR add constraint FK_RELATIONSHIP_8 foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO) on delete restrict on update restrict;

alter table AUXILIAR add constraint FK_RELATIONSHIP_10 foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO) on delete restrict on update restrict;

alter table DOCENTE add constraint FK_RELATIONSHIP_9 foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO) on delete restrict on update restrict;

alter table DOCENTE_MATERIA add constraint FK_RELATIONSHIP_21 foreign key (ID_USUARIO, COD_DOCENTE)
      references DOCENTE (ID_USUARIO, COD_DOCENTE) on delete restrict on update restrict;

alter table DOCENTE_MATERIA add constraint FK_RELATIONSHIP_24 foreign key (ID_MATERIA)
      references MATERIA (ID_MATERIA) on delete restrict on update restrict;

alter table ESTUDIANTE add constraint FK_RELATIONSHIP_11 foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO) on delete restrict on update restrict;

alter table ESTUDIANTE_GRUPO add constraint FK_RELATIONSHIP_19 foreign key (ID_USUARIO, COD_SYS)
      references ESTUDIANTE (ID_USUARIO, COD_SYS) on delete restrict on update restrict;

alter table ESTUDIANTE_GRUPO add constraint FK_RELATIONSHIP_20 foreign key (ID_GRUPO)
      references GRUPO (ID_GRUPO) on delete restrict on update restrict;

alter table ESTUDIANTE_PRACTICA add constraint FK_RELATIONSHIP_22 foreign key (ID_PRACTICA)
      references PRACTICA (ID_PRACTICA) on delete restrict on update restrict;

alter table ESTUDIANTE_PRACTICA add constraint FK_RELATIONSHIP_25 foreign key (ID_USUARIO, COD_SYS)
      references ESTUDIANTE (ID_USUARIO, COD_SYS) on delete restrict on update restrict;

alter table GRUPO add constraint FK_RELATIONSHIP_14 foreign key (ID_MATERIA)
      references MATERIA (ID_MATERIA) on delete restrict on update restrict;

alter table GRUPO add constraint FK_RELATIONSHIP_17 foreign key (DOC_ID_USUARIO, COD_DOCENTE)
      references DOCENTE (ID_USUARIO, COD_DOCENTE) on delete restrict on update restrict;

alter table GRUPO add constraint FK_RELATIONSHIP_18 foreign key (ID_USUARIO, COD_AUXILIAR)
      references AUXILIAR (ID_USUARIO, COD_AUXILIAR) on delete restrict on update restrict;

alter table HORARIO add constraint FK_RELATIONSHIP_23 foreign key (ID_GRUPO)
      references GRUPO (ID_GRUPO) on delete restrict on update restrict;

alter table PRACTICA add constraint FK_RELATIONSHIP_27 foreign key (ID_PRACTICA_TAREA)
      references PRACTICA_TAREA (ID_PRACTICA_TAREA) on delete restrict on update restrict;

alter table PRACTICA_TAREA add constraint FK_RELATIONSHIP_26 foreign key (ID_GRUPO)
      references GRUPO (ID_GRUPO) on delete restrict on update restrict;

alter table SESION add constraint FK_RELATIONSHIP_15 foreign key (ID_USUARIO, COD_SYS)
      references ESTUDIANTE (ID_USUARIO, COD_SYS) on delete restrict on update restrict;

alter table USUARIO add constraint FK_RELATIONSHIP_7 foreign key (ID_ROL)
      references ROL (ID_ROL) on delete restrict on update restrict;

