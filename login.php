<!doctype html>
<html lang="en">
<?php include 'head.php';?>
  <head>
    <title>inicar sesion</title>
  
  </head>

  <body>
    <!-- Formulario Login -->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
          
          <div class="spacing-1"></div>

          <!-- Estructura del formulario -->
          <fieldset>

            <legend class="center">Login</legend>
            <!-- vewrifica conexion y si existe entonces te redirecciona-->
            <form action="vista-rol.php" method="POST"> 
             <!--fin -->
            <!-- Caja de texto para usuario -->
            <label class="sr-only" for="user">Usuario</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="fa fa-user"></i>  <span>*</span></div>
              <!--<input type="text" class="form-control" id="user" placeholder="Ingresa tu correo">-->
              <input type="email" class="form-control" name="email" placeholder="Ingrese su correo"> 
            </div>

            <!-- Div espaciador -->
            <div class="spacing-2"></div>

            <!-- Caja de texto para la clave-->
            <label class="sr-only" for="clave">Contraseña</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="fa fa-lock"></i>  <span>*</span></div>
             
              <input type="password" class="form-control " name="password" placeholder="Ingrese su password" required>
            </div>

                  
             <div style="color:  #0000ff;">Los campos con * son requeridos.</div>
            <!--  )-->
           

            <!-- boton #login para activar la funcion click y enviar el los datos mediante ajax -->
            <div class="row">
              <div class="col-xs-8 col-xs-offset-2">
                <div class="spacing-2"></div>
                <button type="submit" class="btn btn-success btn-block">Login</button>
               
              </form>
              </div>
            </div>

            <section class="text-accent center">
              <div class="spacing-2"></div>
              
              <p>
                No tiene una cuenta? <a href="registro.php"> Registrese</a>
              </p>
            </section>

          </fieldset>
        </div>
      </div>
    </div>

    <!-- / Final Formulario login -->

    
  </body>
</html> 