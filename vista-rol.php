<?php
session_start();
include 'geters.php';
include 'funsest.php';
?>

<!doctype html>
<html lang="en">
	<head>
		<title>Check Login and create session</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	</head>

	<body>
		<div class="container">
		
			<?php
			// Connection info. file
			include 'conn.php';	
			
			// Connection variables
			$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

			// Check connection
			if (!$conn) {
				die("Connection failed: " . mysqli_connect_error());
			}
			
			// data sent from form login
			$email = $_POST['email']; 
			$contra = $_POST['password'];
			
			// Query sent to database
			$result = mysqli_query($conn, "SELECT ID_USUARIO, CORREO, PASSWORD, NOMBRES, ID_ROL FROM usuario WHERE CORREO = '$email'");			
			// Variable $row hold the result of the query
			$row = mysqli_fetch_assoc($result);
			
			// Variable $hash hold the password hash on database
			$hash = $row['PASSWORD'];


			
			/* 
			password_Verify() function verify if the password entered by the user
			match the password hash on the database. If everything is OK the session
			is created for one minute. Change 1 on $_SESSION[start] to 5 for a 5 minutes session.*/
				$hash = $row['PASSWORD'];
				$idrol = $row['ID_ROL'];
				$idus = $row['ID_USUARIO'];
				if(password_verify($_POST['password'], $hash)){
					$_SESSION['loggedin'] = true;
					$_SESSION['id_rol'] = $idrol;
					$_SESSION['correo'] = $row['CORREO'];
          			$_SESSION['id'] = $row['ID_USUARIO'];
          			$_SESSION['name'] = $row['NOMBRES'];

					if($idrol==4){
						//$_SESSION['loggedin'] = true;
						$usuario = obtenerest($idus);
						//registrarsesi($usuario);

						//$_SESSION['name'] = $respuesta['NOMBRES'];
						$_SESSION['start'] = time();
						//$_SESSION['id_rol'] = $idrol;
						$_SESSION['expire'] = $_SESSION['start'] + (1 * 60) ;
						
						header ('Location: estudiante/dashboard.php');
					}else{
						if($idrol==3)
							header('Location: auxiliar/dashboard.php');
						else{
							if($idrol==2)
								header('Location: docente/dashboard.php');
							else {
								header('Location: administrador/dashboard.php');
							}
						}
					}
				}else{
					echo 'No son iguales las contrasenas';
				}

			?>
		</div>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

	</body>
</html>