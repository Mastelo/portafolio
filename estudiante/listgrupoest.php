<?php 

include '../conn.php';
include 'dashboard.php';

  $id = $_SESSION['id']; 
  
  echo $id;


  
  //$sql = "SELECT usuario.NOMBRES,usuario.APELLIDOS, materia.NOMBRE_MATERIA, grupo.NOMBRE_GRUPO, grupo.ID_GRUPO,   FROM grupo, materia, docente, usuario WHERE materia.ID_MATERIA = grupo.ID_MATERIA and grupo.DOC_ID_USUARIO = docente.ID_USUARIO and docente.ID_USUARIO= usuario.ID_USUARIO";
  $sql = "SELECT usuario.NOMBRES,usuario.APELLIDOS, materia.NOMBRE_MATERIA, grupo.NOMBRE_GRUPO, grupo.ID_GRUPO, horario.HORA_ENTRADA , horario.HORA_SALIDA FROM grupo, materia, docente, usuario,horario WHERE materia.ID_MATERIA = grupo.ID_MATERIA and grupo.DOC_ID_USUARIO = docente.ID_USUARIO and docente.ID_USUARIO= usuario.ID_USUARIO and grupo.ID_GRUPO = horario.ID_GRUPO";

  $query = $conn->query($sql);

?>


<div class="content">
        <div id="pad-wrapper" class="form-page">
            <div class="row header">
                <h3>Materias con practica registradas </h3>
            </div>

               <table class="table table-bordered table-hover">
<thead>
    <th>MATERIA</th>
    <th>NOMBRE DEL GRUPO</th>
    <th>Docente</th>
    <th>Hora entrada</th>
    <th>Hora salida</th>
    <th>Opciones</th>
</thead>

<tbody>
  <?php while ($row=$query->fetch_array()):?>
    <tr>
      <td><?php echo $row['NOMBRE_MATERIA']; ?></td>
      <td><?php echo $row['NOMBRE_GRUPO']; ?></td>
      <td><?php echo $row['NOMBRES']." ".$row['APELLIDOS']; ?></td>
      <td><?php echo $row['HORA_ENTRADA']; ?></td>
      <td><?php echo $row['HORA_SALIDA']; ?></td>

      <td style="width:150px;">
    <a href="inscrgrupo.php?id=<?php echo $row["ID_GRUPO"];?>" class="btn btn-sm btn-warning">Inscribirse</a>
    
  </td>

    </tr>
  <?php endwhile;?>
</tbody>

  </table>
 </div>
</div>

