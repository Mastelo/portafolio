<?php
// connect to the database

include '../conn.php';
//session_start(); ver si es necesario
include '../geters.php';

$corre = $_SESSION['correo']; 
$usuario = obtenerdatos($corre);
$id_usu = $usuario['ID_USUARIO']; 

//cargar todo del portafolio del estudiante correspondiente
//ANHADIDO PARA EL DORARGA 
//$sql = "SELECT * FROM portafolio WHERE ID_USUARIO=$id_usu";
//$result = mysqli_query($conn, $sql);
$files = obtenerporta($id_usu);


//$files = mysqli_fetch_all($result, MYSQLI_ASSOC);

// Uploads files
if (isset($_POST['save'])) { // if save button on the form is clicked
    // 1 $nombreDirectorio = $usuario['NOMBRES'].''.$usuario['TELEFONO'];
    // name of the uploaded file
    $filename = $_FILES['myfile']['name'];
   
    $destination = "uploads/{$id_usu}/" . $filename;
    
    // get the file extension
    $extension = pathinfo($filename, PATHINFO_EXTENSION);

    // the physical file on a temporary uploads directory on the server
    $file = $_FILES['myfile']['tmp_name'];
    $size = $_FILES['myfile']['size'];

    if (!in_array($extension, ['zip', 'pdf', 'docx', 'txt', 'rar', 'png'])) {
        echo "La extencion debe ser .zip, .pdf, .docx, .rar o .txt";
    } elseif ($_FILES['myfile']['size'] > 1000000) { // file shouldn't be larger than 1Megabyte
        echo "File too large!";
    } else {
        // move the uploaded (temporary) file to the specified destination
        if (move_uploaded_file($file, $destination)) {

            
            $sql = "INSERT INTO portafolio (ID_USUARIO, NOMBRE, FECHA_CREACION) VALUES ('$id_usu','$filename',NOW())";
            
            if (mysqli_query($conn, $sql)) {

                echo "Exito! <div class='alert alert-danger mt-4' role='alert'>Se subio el archivo <p><a href='verport.php'><strong>Clic para ver los archivos subidos</strong></a></p></div>";
            }
            else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        } else {
            echo "Error al subir el archivo.";
        }
    }
}



// Downloads files
if (isset($_GET['file_IDPORTA'])) {
    $id = $_GET['file_IDPORTA'];

    // fetch file to download from database
    $sql = "SELECT * FROM portafolio WHERE IDPORTA = $id";
    $result = mysqli_query($conn, $sql);

    $file = mysqli_fetch_assoc($result);
    //$filepath = 'uploads/' . $file['NOMBRE'];
    $filepath ="uploads/{$id_usu}/". $file['NOMBRE'];

    if (file_exists($filepath)) {
        echo $file;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($filepath));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
       // header('Content-Length: ' . filesize('uploads/' . $file['NOMBRE']));
        header('Content-Length: ' . filesize("uploads/{$id_usu}/" . $file['NOMBRE']));
       // readfile('uploads/' . $file['NOMBRE']);
        readfile("uploads/{$id_usu}/" . $file['NOMBRE']);

        // Now update downloads count
        //$newCount = $file['downloads'] + 1;
       // $updateQuery = "UPDATE files SET downloads=$newCount WHERE id=$id";
       // mysqli_query($conn, $updateQuery);
        exit;
    }

}