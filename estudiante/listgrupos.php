<?php 

include '../conn.php';
include 'dashboard.php';


    $id = $_SESSION['id']; 
    $sql = "SELECT usuario.NOMBRES,usuario.APELLIDOS, materia.NOMBRE_MATERIA, grupo.NOMBRE_GRUPO, grupo.ID_GRUPO, horario.HORA_ENTRADA , horario.HORA_SALIDA FROM docente, usuario, grupo, materia, estudiante_grupo, horario WHERE materia.id_materia = grupo.id_materia and estudiante_grupo.ID_GRUPO = grupo.ID_GRUPO and grupo.DOC_ID_USUARIO = docente.ID_USUARIO and docente.ID_USUARIO= usuario.ID_USUARIO and grupo.ID_GRUPO = horario.ID_GRUPO and estudiante_grupo.ID_USUARIO='$id' ";

      if (mysqli_query($conn, $sql)) {

            }
            else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }

  $query = $conn->query($sql);
?>

<div class="content">
        <div id="pad-wrapper" class="form-page">
            <div class="row header">
                <h3>Estado de inscripcion </h3>
            </div>
               <table class="table table-bordered table-hover">
<thead>
    <th>MATERIA</th>
    <th>NOMBRE DEL GRUPO</th>
    <th>Docente</th>
    <th>Hora entrada</th>
    <th>Hora salida</th>
    <th>Opciones</th>

</thead>

<tbody>
  <?php while ($row=$query->fetch_array()):?>
    <tr>
      <td><?php echo $row['NOMBRE_MATERIA']; ?></td>
      <td><?php echo $row['NOMBRE_GRUPO']; ?></td>
      <td><?php echo $row['NOMBRES']." ".$row['APELLIDOS']; ?></td>
      <td><?php echo $row['HORA_ENTRADA']; ?></td>
      <td><?php echo $row['HORA_SALIDA']; ?></td>
      
      <td style="width:150px;">
          
          <a href="#" id="del-<?php echo $row["ID_GRUPO"]; ?>" class="btn btn-sm btn-danger">Retirar</a>
          <script>
          $("#del-"+<?php echo $row["ID_GRUPO"];?>).click(function(e){
            e.preventDefault();
            p = confirm("Esta seguro de retirar esta materia?");
            if(p){
              window.location="retirarmateria.php?id="+<?php echo $row["ID_GRUPO"];?>;
            }
          });
          </script>
      </td>

    </tr>
  <?php endwhile;?>
</tbody>

  </table>
 </div>
</div>

